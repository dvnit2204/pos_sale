<?php

namespace App\Services;

use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client as OClient;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Laravel\Passport\Token;
use Laravel\Passport\TokenRepository;
use Laravel\Passport\RefreshTokenRepository;
use Throwable;

class AuthService
{
    protected $oClient;
    protected $guards;

    public function __construct()
    {
        $this->_retrieveClients();
    }

    private function _retrieveClients()
    {
        $clients = OClient::where('password_client', 1)->get();
        $configProviders = config('auth.providers');
        $configGuards = config('auth.guards');

        $providerGuardMaps = [];
        foreach ($configGuards as $guard => $configGuard ) {
            if (isset($configGuard['provider']) && $configGuard['driver'] === 'session' ) {
                $providerGuardMaps[$configGuard['provider']] = $guard;
            }
        }

        foreach ($clients as $client) {
            if (isset($client['provider']) 
            && isset($configProviders[$client['provider']])
            && isset($providerGuardMaps[$client['provider']])
            ) {
                $model = $configProviders[$client['provider']]['model'];
                $this->oClient[$model] = $client;
                $this->guards[$model] = $providerGuardMaps[$client['provider']];
            }
        }
    }
    private function _getClient(string $key)
    {
        return $this->oClient[$key] ?? null;
    }
    private function _getGuard(string $key)
    {
        return $this->guards[$key] ?? null;
    }
    
    public function register($params)
    {
        try{
            \DB::beginTransaction();
            $dataUser = [
                "name" => $params['name'],
                "code" => "management",
                "email" => $params['email'],
                "gender" => $params['gender'],
                "dob" => $params['dob'],
                "status" => config('constants.STATUS_USER_UNACTIVE'),
                "password" => Hash::make('12345678'),
                "is_role" => config('constants.STATUS_USER_CEO')
            ];
            $user = User::create($dataUser);
            Store::create([
                "user_id" => $user->id,
                "name" => $params['name_store'],
            ]);
            
            \DB::commit();
           
            return response()->json([
                'message' => 'Bạn đăng ký thành công. Admin sẽ cấp tài khoản cho bạn'
            ]);

        }catch (Throwable $e){
            \DB::rollBack();
            return $e;
        }
        
    }
    public function login(string $modelNamespace, $email, $password)
    {
        if (!auth($this->_getGuard($modelNamespace))->attempt(compact('email', 'password'))) {
            throw new UnauthorizedHttpException(__('api.exception.user_not_found'));
        }
        $userActive = $modelNamespace::where('email', $email)->where('status', config('constants.STATUS_USER_ISACTIVE'))->first();
        if(!$userActive){
            return response()->json([
                'message' => 'Tài khoản của bạn chưa được cấp phát'
            ]);
        }
        $result = $this->generateToken($modelNamespace, $email, $password);
        if (!$result) {
            throw new UnauthorizedHttpException(__('api.exception.user_not_found'));
        }
        $user = auth($this->_getGuard($modelNamespace))->user();
        $result['user'] = $user;

        return $result;
    }
    public function generateToken(string $modelNamespace, $username, $password)
    {
        $oClient = $this->_getClient($modelNamespace);
        if (!$oClient) return null;
        $request = Request::create('/oauth/token', 'POST', [
            'grant_type' => 'password',
            'client_id' => (string)$oClient->id,
            'client_secret' => $oClient->secret,
            'username' => $username,
            'password' => $password,
            'scope' => '*',
        ]);
        $response = app()->handle($request);
        if ($response->getStatusCode() === HttpResponse::HTTP_OK) {
            return json_decode((string)$response->getContent(), true);
        }
        return null;
    }

      /**
     * @param Token $token
     * @return mixed
     */
    public function revokeToken(Token $token)
    {
        $tokenRepository = app(TokenRepository::class);
        $refreshTokenRepository = app(RefreshTokenRepository::class);
        $tokenRepository->revokeAccessToken($token->id);
        return $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($token->id);
    }
}