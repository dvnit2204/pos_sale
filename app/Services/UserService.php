<?php

namespace App\Services;

use App\Mail\CreateAccountEmail;
use App\Models\Store;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Str;

class UserService extends BaseService
{
    public function model()
    {
        return User::class;
    }

    public function list()
    {
        return $this->model->with(['userRole' => function($qu) {
            $qu->select('*');
        }])->where('id', '<>', auth()->user()->id)->orderBy('id', 'desc')->paginate(10);
    }

    public function createUser($params)
    {
        \DB::beginTransaction();
        try {
            $userData = $this->model->where('id', auth()->user()->id)->with('store')->first();
            $password = Str::random(8);
            $data = [
                "code" => strtoupper($params['code']),
                "name" => $params['name'],
                "email" => $params['email'],
                "phone" => $params['phone'],
                "gender" => $params['gender'],
                "dob" => $params['dob'],
                "status" => config('constants.STATUS_USER_ISACTIVE'),
                "is_role" => config('constants.STATUS_USER_EMP'),
                "password" => Hash::make($password)
            ];
            $user = $this->model->create($data);
            \DB::table('role_user')->insert(['role_id' => $params['role'], 'user_id' => $user->id]);
            Store::create([
                'name' => $userData->store->name,
                'user_id' => $user->id
            ]);
            $data = new \stdClass();
            $data->email = $user->email;
            $data->password = $password;
            $data->sender = $userData->store->name;
            $data->receiver = $user->name;
    
            \Mail::to($user->email)->send(new CreateAccountEmail($data));
            \DB::commit();
            return response()->json([
                'message' => 'Thêm mới nhân viên thành công',
                'data' => $user
            ]);
        } catch (\Throwable $th) {
            dd($th);
            \DB::rollback();
            //throw $th;
        }
    }

    public function updateUser($params, $id)
    {
        \DB::beginTransaction();
        try {
            $userData = $this->model->where('id', $id)->with(['store', 'userRole'])->first();
            \DB::table('role_user')->where('user_id', $userData->id)->delete();
            $data = [
                "code" => strtoupper($params['code']),
                "name" => $params['name'],
                "email" => $params['email'],
                "phone" => $params['phone'],
                "gender" => $params['gender'],
                "dob" => $params['dob'],
            ];
            $user = $userData->update($data);
            \DB::table('role_user')->insert(['role_id' => $params['role'], 'user_id' => $userData->id]);
            Store::create([
                'name' => $userData->store->name,
                'user_id' => $userData->id
            ]);
            \DB::commit();
            return response()->json([
                'message' => 'Sửa nhân viên thành công',
                'data' => $user
            ]);
        } catch (\Throwable $th) {
            dd($th);
            \DB::rollback();
            //throw $th;
        }
    }
    public function updateStatusUser($id)
    {
        return $this->model->where('id', $id)->update([
            'status' => config('constants.STATUS_USER_UNACTIVE')
        ]); 
    }
}