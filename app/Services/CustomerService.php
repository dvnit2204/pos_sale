<?php

namespace App\Services;

use App\Models\Customer;

class CustomerService extends BaseService
{
    public function model()
    {
        return Customer::class;
    }
    public function list()
    {
        return $this->model->query()->orderBy('id', 'desc')->paginate(10);
    }
    public function getDetail($id)
    {
        return $this->model->query()->where('id', $id)->first();
    }
    public function createCustomer($params)
    {
        $data = [
            'code_customer' => strtoupper($params['code_customer']),
            'name' => $params['name'],
            'phone_number' => $params['phone_number'],
            'address' => $params['address'],
            'note' => $params['note']
        ];
        $customer = $this->model->query()->create($data);
        if(empty($params['code_customer'])){
            $str = "KH0000";
            $customer->update([
                'code_customer' => str_pad($str,7, $customer->id)
            ]);
        }
        return $customer;
    }
    public function updateCustomer($params, $id)
    {
        $customer = $this->getDetail($id);
        $data = [
            'code_customer' => strtoupper($params['code_customer']),
            'name' => $params['name'],
            'phone_number' => $params['phone_number'],
            'address' => $params['address'],
            'note' => $params['note']
        ];
        $item = $customer->update($data);
        if(empty($params['code_customer'])){
            $str = "KH0000";
            $customer->update([
                'code_customer' => str_pad($str,7, $customer->id)
            ]);
        }
        return $customer;
    }
}