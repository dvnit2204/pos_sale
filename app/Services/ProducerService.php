<?php

namespace App\Services;

use App\Http\Requests\ProducerRequest;
use App\Models\Producer;

class ProducerService extends BaseService
{
    public function model()
    {
        return Producer::class;
    }

    public function list()
    {
        return $this->model->query()->orderBy('id', 'desc')->get();
    }

    public function listProducer()
    {
        return $this->model->query()->orderBy('id', 'desc')->get();
    }

    public function createProducer($params)
    {
        return $this->model->query()->create([
            'name' => $params['name']
        ]);
    }
  
}