<?php

namespace App\Services;
use App\Models\Supplier;

class SupplierService extends BaseService
{
    public function model()
    {
        return Supplier::class;
    }
    public function list()
    {
        return $this->model->query()->orderBy('id', 'desc')->paginate(10);
    }
    public function getDetail($id)
    {
        return $this->model->query()->where('id', $id)->first();
    }
    public function createSupplier($params)
    {
        $data = [
            'code_supplier' => $params['code_supplier'],
            'name' => $params['name'],
            'phone_number' => $params['phone_number'],
            'address' => $params['address'],
            'note' => $params['note']
        ];
        $customer = $this->model->query()->create($data);
        if(empty($params['code_supplier'])){
            $str = "NCC000";
            $customer->update([
                'code_supplier' => str_pad($str,7, $customer->id)
            ]);
        }
        return $customer;
    }
    public function updateSupplier($params, $id)
    {
        $customer = $this->getDetail($id);
        $data = [
            'code_supplier' => strtoupper($params['code_supplier']),
            'name' => $params['name'],
            'phone_number' => $params['phone_number'],
            'address' => $params['address'],
            'note' => $params['note']
        ];
        $item = $customer->update($data);
        if(empty($params['code_supplier'])){
            $str = "NCC000";
            $customer->update([
                'code_supplier' => str_pad($str,7, $customer->id)
            ]);
        }
        return $customer;
    }
}