<?php

namespace App\Services;

use App\Models\Product;

class ProductService extends BaseService
{
    public function model()
    {
        return Product::class;
    }

    public function list()
    {
        return $this->model->query()->with('producer:id,name', 'category:id,title')->orderBy('id', 'desc')->paginate(10);
    }

    public function listProductGetName()
    {
        return $this->model->query()->select(\DB::raw('CONCAT(code_product, "-", name) as value'), \DB::raw('id as link'))->orderBy('id', 'desc')->paginate(10);
    }

    public function createProduct($params, $image)
    {
        $data = [
            'name' => ucfirst($params['name']),
            'quatity' => $params['quatity'],
            'category_id' => $params['category_id'],
            'price' => $params['price'],
            'price_cost' => $params['price_cost'],
            'producer_id' => $params['producer_id'],
            'status' => $params['status'],
            'image' => $image,
            'code_product' => strtoupper($params['code_product'])
        ];
        $product = $this->model->query()->create($data);
        if(empty($params['code_product'])){
            $str = "SP0000";
            $product->update([
                'code_product' => str_pad($str,7, $product->id)
            ]);
        }
        return $product;
    }
    public function getDetail($id)
    {
        return $this->model->with('producer:id,name', 'category:id,title')->where('id', $id)->first();
    }
    public function updateProduct($params, $id, $image)
    {
        $product = $this->getDetail($id);
        $data = [
            'name' => ucfirst($params['name']),
            'quatity' => $params['quatity'],
            'category_id' => $params['category_id'],
            'price' => $params['price'],
            'price_cost' => $params['price_cost'],
            'producer_id' => $params['producer_id'],
            'status' => $params['status'],
            'image' => $image,
            'code_product' => strtoupper($params['code_product'])
        ];
        $item = $product->update($data);
        if(empty($params['code_product'])){
            $str = "SP0000";
            $product->update([
                'code_product' => str_pad($str,7, $product->id)
            ]);
        }
        return response()->json([
            'message' => 'Cập nhật sản phẩm thành công'
        ]);
    }

}