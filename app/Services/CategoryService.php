<?php

namespace App\Services;

use App\Models\Category;

class CategoryService extends BaseService
{
    public function model()
    {
        return Category::class;
    }
    public function list()
    {
        return $this->model->query()->orderBy('id', 'desc')->get();
    }
    public function listCategory()
    {
        return $this->model->query()->select('id', 'title')->orderBy('id', 'desc')->get();
    }
    public function createCategory($params)
    {
        return $this->model->query()->create([
            'title' => $params['title']
        ]);
    }
  
}