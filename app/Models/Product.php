<?php

namespace App\Models;

use App\Libraries\Common;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';
    protected $fillable=['name', 'code_product', 'quatity', 'category_id', 'price', 'price_cost', 'producer_id', 'image', 'status'];

    public function getImageAttribute($value)
    {
        if(empty($value)){
            return url('images/2022-02-15 15.22.59.jpg');
        }
        return Common::replaceUrlImage($value);
    }
    public function producer()
    {
        return $this->hasOne(Producer::class, 'id', 'producer_id');
    }
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
