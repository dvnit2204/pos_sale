<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\ProducerService;
use App\Services\ProductService;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    protected $productService;
    protected $categoryService;
    protected $producerService;

    function __construct(ProductService $productService, CategoryService $categoryService, ProducerService $producerService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->producerService = $producerService;
    }
    public function listProductGetName()
    {
        return $this->productService->listProductGetName();
    }

    public function listCategory()
    {
        return $this->categoryService->listCategory();
    }
    public function listProducer()
    {
        return $this->producerService->listProducer();
    }
}
