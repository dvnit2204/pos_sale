<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use App\Services\CustomerService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $customerService;

    function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function index()
    {
        return $this->customerService->list();
    }
    public function create(CustomerRequest $request)
    {
        return $this->customerService->createCustomer($request->all());
    }
    public function update(CustomerRequest $request, $id)
    {
        return $this->customerService->updateCustomer($request->all(), $id);
    }
    public function destroy($id)
    {
        Customer::where('id', $id)->delete();
        return response()->json([
            'message' => 'Xóa khách hàng thành công'
        ]);
    }
}
