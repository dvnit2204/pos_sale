<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function getCategory()
    {
        return $this->categoryService->list();
    }
    public function create(CategoryRequest $request)
    {
        return $this->categoryService->createCategory($request->all());
    }
}
