<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProducerRequest;
use App\Services\ProducerService;

class ProducerController extends Controller
{
    protected $producerService;

    function __construct(ProducerService $producerService)
    {
        $this->producerService = $producerService;
    }
    public function getProducer()
    {
        return $this->producerService->list();
    }
    public function create(ProducerRequest $request)
    {
        return $this->producerService->createProducer($request->all());
    }
}
