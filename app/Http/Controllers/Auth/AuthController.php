<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(RegisterRequest $request)
    {
        return $this->authService->register($request->all());
    }

    public function login(LoginRequest $request)
    {
        $data = $request->only('email', 'password');

        $result = $this->authService->login(User::class, $data['email'], $data['password']);
        if (!$result) {
            throw new UnauthorizedHttpException(__('api.exception.user_not_found'));
        }
        return $result;
    }
    public function logout(Request $request)
    {
        $user = auth()->user();
        $this->authService->revokeToken($user->token());

        return response()->json(['message' => 'Đăng xuất thành công']);
    }
    public function profile()
    {
        $user = auth()->user();

        return response()->json(['user' => $user]);
    }
}
