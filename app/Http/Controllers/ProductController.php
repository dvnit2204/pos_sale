<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Libraries\Common;
use App\Models\Product;
use App\Models\User;
use App\Services\ProductService;

class ProductController extends Controller
{
    protected $productService;

    function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        return $this->productService->list();
    }

    public function create(ProductRequest $request)
    {
        $data['image'] = '';
        $user = User::with('store')->where('id', auth()->user()->id)->first();
        $str = date('dmHis', strtotime(now()));
        if($request->hasFile('image')){
            $image = $request->file('image');
            $data['image'] = 'uploads/'.Common::slugify($user->store->name).'/product-' . $str . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('uploads/'.Common::slugify($user->store->name));
            $image->move($destinationPath, $data['image']);
        }
        return $this->productService->createProduct($request->all(), $data['image']);
    }
    public function update(ProductRequest $request, $id)
    {
        $data['image'] = '';
        $user = User::with('store')->where('id', auth()->user()->id)->first();
        $product = Product::where('id', $id)->first();
        $str = date('dmHis', strtotime(now()));
        if($request->hasFile('image')){
            $image = $request->file('image');
            $data['image'] = 'uploads/'.Common::slugify($user->store->name).'/product-' . $str . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('uploads/'.Common::slugify($user->store->name));
            $image->move($destinationPath, $data['image']);
            if ($product->image != "" && file_exists(public_path($product->image))) {
                unlink(public_path($product->image));
            }
        }else{
            $data['image'] = $product->image;
        }
        return $this->productService->updateProduct($request->all(), $id, $data['image']);
    }

    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        return response()->json([
            'message' => 'Xóa sản phẩm thành công!'
        ]);
    }
    public function detail($id)
    {
       return $this->productService->getDetail($id);
    }
}
