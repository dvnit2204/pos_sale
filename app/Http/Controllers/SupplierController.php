<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplierRequest;
use App\Models\Supplier;
use App\Services\SupplierService;

class SupplierController extends Controller
{
    protected $supplierSevice;

    function __construct(SupplierService $supplierSevice)
    {
        $this->supplierSevice = $supplierSevice;
    }

    public function index()
    {
        return $this->supplierSevice->list();
    }
    public function create(SupplierRequest $request)
    {
        return $this->supplierSevice->createSupplier($request->all());
    }
    public function update(SupplierRequest $request, $id)
    {
        return $this->supplierSevice->updateSupplier($request->all(), $id);
    }
    public function destroy($id)
    {
        Supplier::where('id', $id)->delete();
        return response()->json([
            'message' => 'Xóa khách hàng thành công'
        ]);
    }
}
