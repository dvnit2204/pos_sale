<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    public function index()
    {
        return $this->userService->list();
    }
    public function create(UserRequest $request)
    {
        return $this->userService->createUser($request->all());
    }
    public function updateData(UserRequest $request)
    {
        return $this->userService->updateUser($request->all(), $request->id);
    }
    public function updateStatus($id)
    {
        return $this->userService->updateStatusUser($id);
    }
}
