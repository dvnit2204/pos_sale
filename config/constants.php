<?php
return [
    "ROLE_TYPE" => [
        "ceo" => 1,
        "admin" => 2,
        "employee" => 3
    ],
    "ROLE_NAME" => ['ceo'=>'ceo', 'admin'=> 'admin', 'emp'=>'employee'],
    "PERMISSION" => [
        "all_permission" => 1
    ],
    "STATUS_USER_UNACTIVE"=> 0,
    "STATUS_USER_ISACTIVE" => 1,
    "STATUS_USER_CEO" => 1,
    "STATUS_USER_EMP" => 2,
    "ACTIVE_STORE" => 0,
    "UNACTIVE_STORE" => 1
];