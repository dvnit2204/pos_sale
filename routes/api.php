<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ProducerController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/', function () {
    return response()->json(['status' => 'OK']);
});
Route::prefix('auth')->group(function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('forgot-password', [AuthController::class, 'forgotPassword']);
});
Route::middleware('auth:users_api')->group(function () {
    Route::prefix('auth')->group(function() {
        Route::post('logout', [AuthController::class, 'logout']);
        Route::get('/profile', [AuthController::class, 'profile']);
    });
    Route::prefix('common')->group(function() {
        Route::get('products', [CommonController::class, 'listProductGetName']);
        Route::get('categories', [CommonController::class, 'listCategory']);
        Route::get('producers', [CommonController::class, 'listProducer']);
    });
    Route::prefix('users')->group(function() {
        Route::get('/', [UserController::class, 'index']);
        Route::post('/create', [UserController::class, 'create']);
        Route::post('/{id}/update', [UserController::class, 'updateData']);
        Route::post('/{id}/update-status', [UserController::class, 'updateStatus']);
    });

    Route::prefix('categories')->group(function() {
        Route::get('/', [CategoryController::class, 'getCategory']);
        Route::post('/create', [CategoryController::class, 'create']);
    });
    Route::prefix('producers')->group(function() {
        Route::get('/', [ProducerController::class, 'getProducer']);
        Route::post('/create', [ProducerController::class, 'create']);
    });
    Route::prefix('products')->group(function() {
        Route::get('/', [ProductController::class, 'index']);
        Route::post('/create', [ProductController::class, 'create']);
        Route::post('/{id}/update', [ProductController::class, 'update']);
        Route::delete('{id}/delete', [ProductController::class, 'destroy']);
        Route::get('/{id}/detail', [ProductController::class, 'detail']);
    });
    Route::prefix('customers')->group(function() {
        Route::get('/', [CustomerController::class, 'index']);
        Route::post('/create', [CustomerController::class, 'create']);
        Route::post('/{id}/update', [CustomerController::class, 'update']);
        Route::delete('/{id}/delete', [CustomerController::class, 'destroy']);
    });
    Route::prefix('suppliers')->group(function() {
        Route::get('/', [SupplierController::class, 'index']);
        Route::post('/create', [SupplierController::class, 'create']);
        Route::post('/{id}/update', [SupplierController::class, 'update']);
        Route::delete('/{id}/delete', [SupplierController::class, 'destroy']);
    });
});
