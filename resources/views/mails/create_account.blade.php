Hello <i>{{ $data->receiver }}</i>,
<p>Chào mừng bạn đã đến làm việc tại cửa hàng {{ $data->sender }}</p>
  
<p><u>Thông tin tài khoản của quý khách:</u></p>
  
<div>
<p><b>Tài khoản:</b>&nbsp;{{ $data->email }}</p>
<p><b>Mật khẩu:</b>&nbsp;{{ $data->password }}</p>
</div>
<p>Cảm ơn quý khách hàng đã đồng hành cùng chúng tôi.</p>
Thank You,
<br/>
<i>{{ $data->sender }}</i>